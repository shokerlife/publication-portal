import {Resolve} from '@angular/router';
import {User} from './models/user';
import {UserService} from './users/service/user.service';
import {Injectable} from '@angular/core';

@Injectable()
export class UserListResolve implements Resolve<User[]> {

  constructor(private userService: UserService) {
  }

  resolve() {
    return this.userService.getAllUsers();
  }

}
