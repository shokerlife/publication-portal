import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PrincipalService} from '../shared/auth/principal.service';
import {User} from '../models/user';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavBarComponent implements OnInit {
  user$: Observable<User>;

  constructor(private principal: PrincipalService) { }

  ngOnInit() {
    this.user$ = this.principal.getUser();
  }

  logout(): void {
    this.principal.logout();
  }

}
