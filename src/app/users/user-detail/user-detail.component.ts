import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../service/user.service';
import {PrincipalService} from '../../shared/auth/principal.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user: User;
  success: boolean;
  userForm: FormGroup;
  private subscription: Subscription;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private userService: UserService,
              private principalService: PrincipalService) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['user'];

    this.userForm = this.fb.group({
      id: [{ value: this.user.id,
        disabled: true }],
      email: [this.user.email],
      firstName: [this.user.firstName],
      lastName: [this.user.lastName],
      password: [this.user.password],
      role: [this.user.role],
      disabled: [this.user.disabled],
    });

    this.subscription = this.principalService.isAdmin().subscribe((isAdmin) => {
      if (!isAdmin) {
        this.userForm.disable();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  editUser(): void {
    const userToUpdate = this.userService.mapUserToUserProfile(Object.assign({}, this.user, this.userForm.value));
    this.userService.updateUser(userToUpdate).subscribe((response) => this.success = response.status === 200);
  }

}
