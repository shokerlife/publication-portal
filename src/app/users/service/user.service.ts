import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {User} from '../../models/user';
import {Observable} from 'rxjs/Observable';
import {UserProfile, UserRole} from '../../models/request';

export interface Response<T> {
  status: number;
  body: T;
}

@Injectable()
export class UserService {

  private readonly USER_URL = '/api/user/';

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get<UserProfile[]>(`${this.USER_URL}/all`)
      .map(users => this.mapUserProfileListToUserList(users));
  }

  getUserById(userId: string): Observable<User> {
    return this.http.get<UserProfile>(`${this.USER_URL}/${userId}`)
      .map(user => this.mapUserProfileToUser(user));
  }

  updateUser(user: UserProfile): Observable<Response<User>> {
    return this.http.put<UserProfile>(`${this.USER_URL}/${user.id}`, user, {observe: 'response'})
      .map(response => ({
        status: response.status,
        body: this.mapUserProfileToUser(response.body)
      }));
  }

  mapUserProfileListToUserList(users: UserProfile[]): User[] {
    return users.map(user => this.mapUserProfileToUser(user));
  }

  mapUserToUserProfile(user: User): UserProfile {
    const { id, email, firstName, lastName, password, detailsId, disabled} = user;
    return {
      id,
      email,
      password,
      disabled,
      userRole: UserRole.REGULAR,
      userDetails: {
        id: detailsId,
        firstName,
        lastName
      }
    };
  }

  mapUserProfileToUser(userProfile: UserProfile): User {
    const { id, email, password, disabled } = userProfile;
    const { firstName, lastName } = userProfile.userDetails;
    return {
      id,
      email,
      password,
      firstName,
      lastName,
      disabled,
      role: userProfile.userRole,
      detailsId: userProfile.userDetails.id
    };
  }

}
