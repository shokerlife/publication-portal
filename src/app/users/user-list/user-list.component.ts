import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserListComponent implements OnInit {

  users: User[];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.users = this.route.snapshot.data['users'];
  }

}
