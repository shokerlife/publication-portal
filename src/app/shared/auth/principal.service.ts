import {Injectable} from '@angular/core';
import {AccountService} from '../../authorization/service/account.service';
import {User} from '../../models/user';
import {Observable} from 'rxjs/Observable';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {UserRole} from '../../models/request';
import 'rxjs/add/operator/map';
import {LocalStorageService} from 'angular-2-local-storage';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/do';

@Injectable()
export class PrincipalService {

  private readonly localStorageKeyName = 'userId';

  private user$ = new ReplaySubject<User>(1);

  constructor(private accountService: AccountService,
              private localStorageService: LocalStorageService) {
    this.getUserFromStorage();
  }

  private getUserFromStorage() {
    const userId = this.localStorageService.get<string>(this.localStorageKeyName);
    if (userId) {
      this.accountService.getUser(userId).subscribe(this.handleLogin.bind(this));
    } else {
      this.user$.next(null);
    }
  }

  login(email: string, password: string): Observable<User> {
    return this.accountService.login(email, password)
      .do(this.handleLogin.bind(this));
  }

  private handleLogin(user): void {
    this.localStorageService.set(this.localStorageKeyName, user.id);
    this.user$.next(user);
  }

  getUser(): Observable<User> {
    return this.user$.asObservable();
  }

  isAdmin(): Observable<boolean> {
    return this.getUser()
      .map((user: User) => user ? user.role.toString() === UserRole[UserRole.ADMIN].toString() : false);
  }

  logout(): void {
    this.localStorageService.remove(this.localStorageKeyName);
    this.user$.next(null);
  }
}
