import {Directive, OnDestroy, TemplateRef, ViewContainerRef} from '@angular/core';
import {PrincipalService} from './principal.service';
import {Subscription} from 'rxjs/Subscription';

@Directive({
  selector: '[appIsAdmin]'
})
export class HasAnyRoleDirective implements OnDestroy {

  private subscription: Subscription;

  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private principal: PrincipalService) {

    this.subscription = principal.isAdmin().subscribe((isAdmin) => {
      if (isAdmin) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
