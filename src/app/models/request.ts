export interface UserProfile {
  id?: number;
  email: string;
  userRole: UserRole;
  userDetails: UserDetails;
  password: string;
  disabled?: boolean;
}

export interface UserDetails {
  id: number;
  firstName: string;
  lastName: string;
}

export enum UserRole {
  ADMIN, REGULAR
}
