import {UserRole} from './request';
export class User {
  id?: number;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  disabled: boolean;
  role: UserRole;
  detailsId: number;
}
