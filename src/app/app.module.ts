import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AuthorizationModule} from './authorization/authorization.module';
import {AppRoutingModule} from './/app-routing.module';
import {AccountService} from './authorization/service/account.service';
import {HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {PrincipalService} from './shared/auth/principal.service';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {LocalStorageModule} from 'angular-2-local-storage';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UserListComponent} from './users/user-list/user-list.component';
import {UserService} from './users/service/user.service';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import {UserResolve} from './UserResolve';
import {UserListResolve} from './UserListResolve';
import {ReactiveFormsModule} from '@angular/forms';
import { HasAnyRoleDirective } from './shared/auth/is-admin.directive';
import { RubricComponent } from './rubric/rubric-detail/rubric.component';
import { RubricListComponent } from './rubric/rubric-list/rubric-list.component';
import { PublicationListComponent } from './publications/publication-list/publication-list.component';
import { PublicationDetailComponent } from './publications/publication-detail/publication-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent,
    UserListComponent,
    UserDetailComponent,
    HasAnyRoleDirective,
    RubricComponent,
    RubricListComponent,
    PublicationListComponent,
    PublicationDetailComponent
  ],
  imports: [
    NgbModule.forRoot(),
    AuthorizationModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    }),
    ReactiveFormsModule
  ],
  providers: [
    AccountService,
    PrincipalService,
    UserService,
    UserResolve,
    UserListResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
