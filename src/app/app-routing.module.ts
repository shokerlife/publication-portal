import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './authorization/login/login.component';
import {RegistrationComponent} from './authorization/registration/registration.component';
import {HomeComponent} from './home/home.component';
import {UserListComponent} from './users/user-list/user-list.component';
import {UserDetailComponent} from './users/user-detail/user-detail.component';
import {UserResolve} from './UserResolve';
import {UserListResolve} from './UserListResolve';
import {RubricComponent} from './rubric/rubric-detail/rubric.component';
import {RubricListComponent} from './rubric/rubric-list/rubric-list.component';
import {PublicationListComponent} from './publications/publication-list/publication-list.component';
import {PublicationDetailComponent} from './publications/publication-detail/publication-detail.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'users', component: UserListComponent, resolve: {users: UserListResolve} },
  { path: 'rubrics', component: RubricListComponent},
  { path: 'rubrics/:id', component: RubricComponent},
  { path: 'publications', component: PublicationListComponent},
  { path: 'publications/:id', component: PublicationDetailComponent},
  { path: 'users/:id', component: UserDetailComponent, resolve: { user: UserResolve } },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
