import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-publication-list',
  templateUrl: './publication-list.component.html',
  styleUrls: ['./publication-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PublicationListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
