import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-publication-detail',
  templateUrl: './publication-detail.component.html',
  styleUrls: ['./publication-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PublicationDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
