import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../models/user';
import {AccountService} from '../service/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegistrationComponent implements OnInit {
  user: User = new User();

  constructor(private accountService: AccountService,
              private router: Router) { }

  ngOnInit() {
  }

  signUp(): void {
    this.accountService.registerUser(this.user).subscribe(
      () => this.router.navigate(['home'])
    );
  }

}
