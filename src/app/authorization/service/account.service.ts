import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../../models/user';
import {UserProfile} from '../../models/request';
import {UserService} from '../../users/service/user.service';

@Injectable()
export class AccountService {

  private readonly REGISTRATION_URL = '/api/user';

  constructor(private http: HttpClient,
              private userService: UserService) {
  }

  registerUser(user: User): Observable<User> {
    const userProfile = this.userService.mapUserToUserProfile(user);

    return this.http.post<UserProfile>(this.REGISTRATION_URL, userProfile)
      .map(this.userService.mapUserProfileToUser);
  }

  login(email: string, password: string): Observable<User> {
    return this.http.post<UserProfile>(`${this.REGISTRATION_URL}/signin`, {email, password})
      .map(this.userService.mapUserProfileToUser);
  }

  getUser(userId: string): Observable<User> {
    return this.http.get(`${this.REGISTRATION_URL}/${userId}`)
      .map(this.userService.mapUserProfileToUser);
  }
}
