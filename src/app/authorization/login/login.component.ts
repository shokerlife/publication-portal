import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../models/user';
import {Router} from '@angular/router';
import {PrincipalService} from '../../shared/auth/principal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  user: User = new User();
  errorMessage = false;


  constructor(private principal: PrincipalService,
              private router: Router,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  signIn(): void {
    this.principal
      .login(this.user.email, this.user.password)
      .subscribe(
      () => this.router.navigate(['home']),
      (error) => {
        this.errorMessage = error.status === 401;
        setTimeout(() => {
          this.errorMessage = false;
          this.cd.markForCheck();
        }, 5000);
        this.cd.markForCheck();
      }
    );
  }

}
