import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {User} from './models/user';
import {UserService} from './users/service/user.service';
import {Injectable} from '@angular/core';

@Injectable()
export class UserResolve implements Resolve<User> {

  constructor(private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.userService.getUserById(route.paramMap.get('id'));
  }

}
