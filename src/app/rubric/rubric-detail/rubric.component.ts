import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-rubric',
  templateUrl: './rubric.component.html',
  styleUrls: ['./rubric.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RubricComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
