import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-rubric-list',
  templateUrl: './rubric-list.component.html',
  styleUrls: ['./rubric-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RubricListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
